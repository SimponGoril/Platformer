import pygame, sys, os
from classes.colors import BLACK, WHITE, RED
from classes.enemy import Sarge, Grunt, Rat, Brain
from classes.corpse import Corpse
from classes.player import Player
from classes.wall import Wall
from classes.grass import Grass
from classes.door import Door
from classes.powerup import Key, Health, Ammo, Cash
from classes.weapon import Pistol, Shotgun, Sniper
from classes.levels import Level
from classes.highscores import Highscores
from pygame.locals import *
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Game:

    FPS = 30
    BOARD_WIDTH = 800
    BOARD_HEIGHT = 600
    INITIAL_LEVEL = 0
    SPLASHSCREEN = pygame.image.load(os.path.join("assets/environment/", "splashscreen.png"))
    ICON = pygame.image.load(os.path.join("assets/", "icon32x32.png"))

    def __init__(self):
        print("Initializing game...")
        pygame.init()
        pygame.mixer.init()
        pygame.display.set_caption("Platformer")
        pygame.display.set_icon(Game.ICON)
        self.font = pygame.font.SysFont("trebuchet", 40)
        self.fpsClock = pygame.time.Clock()
        self.level = Game.INITIAL_LEVEL
        self.gameOverSound = pygame.mixer.Sound("assets/sounds/gameover.wav")
        self.gameboard = pygame.display.set_mode((Game.BOARD_WIDTH, Game.BOARD_HEIGHT))
        self.universalSpriteGroup = pygame.sprite.Group()
        self.enemiesSpriteGroup = pygame.sprite.Group()
        self.bulletsSpriteGroup = pygame.sprite.Group()
        self.wallsSpriteGroup = pygame.sprite.Group()
        self.powerupSpriteGroup = pygame.sprite.Group()
        self.decalsSpriteGroup = pygame.sprite.Group()
        self.splashscreen = Game.SPLASHSCREEN
        self.player = Player(50, 50, self.wallsSpriteGroup, self.universalSpriteGroup)
        self.door = None
        self.finished = False
        print("Initializing successful")

    def redraw(self):
        self.universalSpriteGroup.draw(self.gameboard)
        self.drawUi()
        self.drawGameOverMessage()
        self.showEnemyHealth()
        if self.player.alive():
            self.gameboard.blit(self.player.image, self.player.rect)
            if self.player.asset:
                self.gameboard.blit(self.player.asset.image, self.player.asset.rect)
        pygame.display.flip()

    def drawUi(self):
        #draw status screen
        pygame.draw.rect(self.gameboard, WHITE, (5, 5, 420, 30))
        self.gameboard.blit(self.font.render("Level: " + str(self.level + 1) + " Score: " + str(self.player.score), True, BLACK), (10, 10))

        #draw inventory
        x = 300
        if self.player.inventory:
            for asset in self.player.inventory:
                self.gameboard.blit(asset.originalImage, (x, 0))
                if asset == self.player.asset:
                    pygame.draw.rect(self.gameboard, RED, (x, 0, 40, 40), 2)
                x += 40

            #draw ammo
            x = 380
            for ammo in range(self.player.asset.ammo):
                x += 40
                self.gameboard.blit(self.player.asset.ammoImage, (x, 0))

        #draw HP
        x = 570
        for heart in range(self.player.health + 1):
            x += 50
            self.gameboard.blit(Health.IMAGE, (x, 0))

        #draw key
        x = 760
        if not self.door.locked:
            self.gameboard.blit(Key.IMAGE, (x, 0))

    def drawGameOverMessage(self):
        if not self.player.alive():
            pygame.draw.rect(self.gameboard, WHITE, (170, 285, 510, 50))
            self.gameboard.blit(self.font.render("GAME OVER press r to RESTART", True, BLACK), (205, 300))

    def showEnemyHealth(self):
        for enemy in self.enemiesSpriteGroup:
            if enemy.health != enemy.maxHealth:
                currentHealthPercentile = (float(enemy.health) / float(enemy.maxHealth)) * 100
                pygame.draw.rect(self.gameboard, WHITE, (enemy.rect.left, enemy.rect.top, 40, 5))
                pygame.draw.rect(self.gameboard, RED, (enemy.rect.left, enemy.rect.top, (currentHealthPercentile * 40) / 100, 5))

    def collisionDetection(self):
        # self.player x enemy
        if self.player.alive() and pygame.sprite.spritecollideany(self.player, self.enemiesSpriteGroup, False):
            for enemy in self.enemiesSpriteGroup:
                if enemy.rect.colliderect(self.player):
                    print("Player took %s damage, current health is %s/3" % (enemy.contactDamage, self.player.health))
                    enemy.die()
                    self.player.health -= enemy.contactDamage
            if self.player.health < 0:
                print("Player was killed")
                self.player.kill()
                self.gameOverSound.play()

        # self.player x door
        if self.player.rect.colliderect(self.door.rect):
            if not self.door.locked:
                print("Level finished")
                self.door.ascend()
                self.level += 1
                self.createLevel(self.level)

        # bullets x enemies
        collisions = pygame.sprite.groupcollide(self.bulletsSpriteGroup, self.enemiesSpriteGroup, False, False, False)
        for bullets, enemies in collisions.items():
            for enemy in enemies:
                if enemy.takeDamage(bullets.damage):
                    print("Enemy %s was killed, adding %s score bounty" % (enemy, enemy.bounty))
                    self.player.score += enemy.bounty
                    if enemy.corpse:
                        self.decalsSpriteGroup.add(Corpse(enemy.corpse, enemy.rect.centerx, enemy.rect.centery - 20, self.universalSpriteGroup))
                bullets.kill()

        # player x powerups
        if pygame.sprite.spritecollideany(self.player, self.powerupSpriteGroup, False):
            for powerup in self.powerupSpriteGroup:
                if self.player.rect.colliderect(powerup.rect):
                    print("%s powerup collected" % powerup)
                    if isinstance(powerup, Ammo):
                        if self.player.asset:
                            self.player.asset.ammo = self.player.asset.maxAmmo
                    elif isinstance(powerup, Health):
                        if self.player.health < 2:
                            self.player.health += 1
                    elif isinstance(powerup, Key):
                        self.door.unlock(True)
                    elif isinstance(powerup, Pistol):
                        self.player.acquireWeapon(Pistol(self.player.rect.centerx - 20, powerup.rect.centery - 20, self.wallsSpriteGroup, self.universalSpriteGroup))
                    elif isinstance(powerup, Shotgun):
                        self.player.acquireWeapon(Shotgun(self.player.rect.centerx - 20, powerup.rect.centery - 20, self.wallsSpriteGroup, self.universalSpriteGroup))
                    elif isinstance(powerup, Sniper):
                        self.player.acquireWeapon(Sniper(self.player.rect.centerx - 20, powerup.rect.centery - 20, self.wallsSpriteGroup, self.universalSpriteGroup))
                    elif isinstance(powerup, Cash):
                        self.player.score += powerup.value
                    powerup.obtained()
                    powerup.kill()

    def showSplashscreen(self):
        while True:
            self.gameboard.blit(self.splashscreen, self.splashscreen.get_rect())
            pygame.display.flip()
            pygame.event.clear()
            keypress = pygame.event.wait()
            if keypress.type == QUIT:
                pygame.quit()
                sys.exit()
            elif keypress.type == KEYDOWN:
                return

    def showEndscreen(self):
        scores = Highscores.loadHighScores()
        scores.append(self.player.score)
        scores.sort(reverse=True)
        Highscores.saveHighScores(scores)

        while True:
            pygame.draw.rect(self.gameboard, BLACK, (0, 0, 800, 600))
            self.gameboard.blit(self.font.render("Congratulations, you finished the game with score:", True, WHITE), (50, 90))
            self.gameboard.blit(self.font.render(str(self.player.score), True, WHITE), (390, 140))
            self.gameboard.blit(self.font.render("HIGHSCORES:", True, RED), (300, 250))
            for i in range(0, len(scores[:10])):
                self.gameboard.blit(self.font.render("%i. %i" % (i + 1, scores[i]), True, WHITE), (320, 280 + i * 25))
            pygame.display.flip()
            pygame.event.clear()
            keypress = pygame.event.wait()
            if keypress.type == QUIT:
                pygame.quit()
                sys.exit()
            elif keypress.type == KEYDOWN:
                return

    def createLevel(self, number):
        if number >= len(Level.level):
            self.finished = True
            return

        print("Creating level %s" % (number + 1))

        for wall in self.wallsSpriteGroup:
            wall.kill()
        for enemy in self.enemiesSpriteGroup:
            enemy.kill()
        for item in self.powerupSpriteGroup:
            item.kill()
        for decal in self.decalsSpriteGroup:
            decal.kill()

        board = Level.level[number]

        x = y = 0
        for row in board:
            for col in row:
                if col == "W":
                    Wall(x, y, self.wallsSpriteGroup)
                else:
                    Grass(x, y, self.universalSpriteGroup)
                if col == "A":
                    Ammo(x, y, self.powerupSpriteGroup)
                if col == "H":
                    Health(x, y, self.powerupSpriteGroup)
                if col == "1":
                    Pistol(x, y, self.wallsSpriteGroup, self.powerupSpriteGroup, self.universalSpriteGroup)
                if col == "2":
                    Shotgun(x, y, self.wallsSpriteGroup, self.powerupSpriteGroup, self.universalSpriteGroup)
                if col == "3":
                    Sniper(x, y, self.wallsSpriteGroup, self.powerupSpriteGroup, self.universalSpriteGroup)
                if col == "G":
                    Grunt(x, y, self.wallsSpriteGroup, self.enemiesSpriteGroup)
                if col == "S":
                    Sarge(x, y, self.wallsSpriteGroup, self.enemiesSpriteGroup)
                if col == "R":
                    Rat(x, y, self.wallsSpriteGroup, self.enemiesSpriteGroup)
                if col == "B":
                    Brain(x, y, self.wallsSpriteGroup, self.enemiesSpriteGroup)
                if col == "K":
                    Key(x, y, self.powerupSpriteGroup)
                if col == "C":
                    Cash(x, y, self.powerupSpriteGroup)
                if col == "X":
                    self.door = Door(x, y, game.universalSpriteGroup)
                if col == "P":
                    self.player.rect.x = x
                    self.player.rect.y = y
                x += 40
            y += 40
            x = 0
        self.universalSpriteGroup.add(self.enemiesSpriteGroup)
        self.universalSpriteGroup.add(self.wallsSpriteGroup)
        self.universalSpriteGroup.add(self.powerupSpriteGroup)

    def restartLevel(self):
        print("Restarting game")
        self.door.unlock(True)
        self.player.resetStats()
        self.player.kill()
        self.player = Player(50, 50, game.wallsSpriteGroup, game.universalSpriteGroup)
        self.level = 0
        self.createLevel(self.level)

    def animateGame(self):
        for bullet in self.bulletsSpriteGroup:
            bullet.fly()
        for enemy in self.enemiesSpriteGroup:
            # if enemy create new asset while traversing, add it to groups
            loopback = enemy.traverse()
            if loopback:
                self.universalSpriteGroup.add(loopback)
                self.enemiesSpriteGroup.add(loopback)

#Public static void main
game = Game()
game.showSplashscreen()
level = 0
game.createLevel(level)

while not game.finished:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if game.player.alive():
            if event.type == KEYDOWN:
                if event.key == pygame.K_1:
                    game.player.selectWeapon(1)
                if event.key == pygame.K_2:
                    game.player.selectWeapon(2)
                if event.key == pygame.K_3:
                    game.player.selectWeapon(3)
                if event.key == pygame.K_SPACE:
                    newBullet = game.player.shoot()
                    if newBullet:
                        game.bulletsSpriteGroup.add(newBullet)
        if event.type == KEYDOWN and event.key == pygame.K_r:
            game.restartLevel()
        if event.type == KEYDOWN and event.key == pygame.K_ESCAPE:
            pygame.quit()
            sys.exit()

    if game.player.alive():
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            game.player.move(-game.player.speed, 0)
        elif keys[pygame.K_RIGHT]:
            game.player.move(game.player.speed, 0)
        else:
            game.player.isMoving = False
        if keys[pygame.K_UP]:
            game.player.move(0, -game.player.speed)
        elif keys[pygame.K_DOWN]:
            game.player.move(0, game.player.speed)

    game.universalSpriteGroup.update()
    game.collisionDetection()
    game.redraw()
    game.animateGame()
    game.fpsClock.tick(Game.FPS)

game.showEndscreen()
print("Exiting...")





