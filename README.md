# PLATFORMER
A retro game written in Python and PyGame framework. Contains multiple animated enemies, 
several weapons a couple of levels and final BOSS. Download / fork the repository and execute game.exe file to START

![Screenshot](https://i.imgur.com/WGuvAZi.jpg')

## How to run it

### Python
  1. Clone repository
  ```
  git clone https://github.com/SimponGoril/Platformer.git
  ```
  2. Install dependencies
  ```
  py setup.py install
  ```
  3. Execute Game.py
  ```
  py game.py
  ```
### Windows executable
Use game.exe to start the latest game build
## Rules
  1. Get key to open door to next level 
  2. Avoid or kill goblins 
  3. Collect Powerups to get new weapons, health or ammo

## Controls
  * Arrow keys - Directional movement
  * Spacebar - Shoot
  * Key 'r' - Restart
  * Number keys - Select weapon in inventory (if availible)
  * Esc - Exit the game
  
## Building EXE runnable
After making desired changes to original code, run this line of code to build updated exe file
```
    py setup.py build
```