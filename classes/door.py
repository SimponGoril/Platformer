import pygame, sys, os
from classes.powerup import Powerup
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


# noinspection PyAttributeOutsideInit
class Door(Powerup):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/environment/", "door40x40closed.png"))
    ANIMATION = ""

    def __init__(self, x, y, *spriteGroup):
        super(Door, self).__init__(x, y, Door.SIZE, Door.IMAGE, Door.ANIMATION, *spriteGroup)
        self.locked = True
        self.ascendSound = pygame.mixer.Sound("assets/sounds/ascend.wav")

    def ascend(self):
        self.ascendSound.play()

    def unlock(self, locked):
        if locked:
            self.image = pygame.image.load(os.path.join("assets/environment/", "door40x40open.png"))
            self.locked = False
        else:
            self.image = pygame.image.load(os.path.join("assets/environment/", "door40x40closed.png"))
            self.locked = True

