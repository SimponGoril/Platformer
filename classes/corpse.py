import sys, os
from classes.sprite import Sprite
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Corpse(Sprite):

    SIZE = 40
    ANIMATION = ""

    def __init__(self, image, x, y, *spriteGroup):
        super(Corpse, self).__init__(x, y, Corpse.SIZE, image, Corpse.ANIMATION, *spriteGroup)
