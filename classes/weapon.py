import pygame, sys, os, random
from classes.sprite import Sprite
from classes.bullet import Bullet
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Weapon(Sprite):

    def __init__(self, x, y, size, image, animation, shotSound, maxAmmo, wallspriteGroup, *spriteGroup):
        super(Weapon, self).__init__(x, y, size, image, animation, *spriteGroup)
        self.wallsSpriteGroup = wallspriteGroup
        self.shotSound = shotSound
        self.originalImage = image
        self.obtainSound = pygame.mixer.Sound("assets/sounds/obtain.wav")
        self.ammo = 0
        self.maxAmmo = maxAmmo
        self.firing = False
        self.flashDuration = 0

    def obtained(self):
        self.obtainSound.play()

    def flash(self):
        self.image = self.flashImage if self.horizontal == "right" else pygame.transform.flip(self.flashImage, True, False)
        self.firing = True

    def update(self):
        if self.firing:
            self.flashDuration += 14
            if self.flashDuration >= 100:
                self.flashDuration = 0
                self.image = self.originalImage if self.horizontal == "right" else pygame.transform.flip(self.originalImage, True, False)


#Basic weapon
class Pistol(Weapon):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/weapons/pistol/", "pistol40x40a.png"))
    ANIMATION = ""
    FLASH = pygame.image.load(os.path.join("assets/weapons/pistol/", "pistol40x40b.png"))
    DAMAGE = 1
    MAX_AMMO = 5
    RANGE = 250
    SPEED = 23

    def __init__(self, x, y, powerupSpriteGroup, wallsSpriteGroup, *spriteGroup):
        super(Pistol, self).__init__(x, y, Pistol.SIZE, Pistol.IMAGE, Pistol.ANIMATION, pygame.mixer.Sound("assets/sounds/pistol.wav"), Pistol.MAX_AMMO, powerupSpriteGroup, wallsSpriteGroup, *spriteGroup)
        self.ammoImage = pygame.image.load(os.path.join("assets/weapons/pistol/", "bullet40x40.png"))
        self.ammo = Pistol.MAX_AMMO
        self.flashImage = Pistol.FLASH

    def shoot(self):
        if self.ammo > 0:
            self.ammo -= 1
            self.shotSound.play()
            self.flash()
            return Bullet(Pistol.SPEED, Pistol.RANGE, Pistol.DAMAGE, self.rect.centerx, self.rect.centery, self.horizontal, random.randint(-1, 1), self.wallsSpriteGroup, self.spriteGroup)


#Stronger than pistol - shoots several bullets at once
class Shotgun(Weapon):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/weapons/shotgun/", "shotgun40x40a.png"))
    ANIMATION = ""
    FLASH = pygame.image.load(os.path.join("assets/weapons/shotgun/", "shotgun40x40b.png"))
    DAMAGE = 1
    MAX_AMMO = 5
    RANGE = 150
    SPEED = 33

    def __init__(self, x, y, powerupSpriteGroup, wallsSpriteGroup, *spriteGroup):
        super(Shotgun, self).__init__(x, y, Shotgun.SIZE, Shotgun.IMAGE, Shotgun.ANIMATION, pygame.mixer.Sound("assets/sounds/shotgun.wav"), Shotgun.MAX_AMMO, powerupSpriteGroup, wallsSpriteGroup, *spriteGroup)
        self.ammoImage = pygame.image.load(os.path.join("assets/weapons/shotgun/", "shell40x40.png"))
        self.ammo = Shotgun.MAX_AMMO
        self.flashImage = Shotgun.FLASH

    def shoot(self):
        if self.ammo > 0:
            self.ammo -= 1
            self.shotSound.play()
            self.flash()
            bullets = [Bullet(Shotgun.SPEED, Shotgun.RANGE, Shotgun.DAMAGE, self.rect.centerx, self.rect.centery, self.horizontal, -5, self.wallsSpriteGroup, self.spriteGroup),
                       Bullet(Shotgun.SPEED, Shotgun.RANGE, Shotgun.DAMAGE, self.rect.centerx, self.rect.centery, self.horizontal, 0, self.wallsSpriteGroup, self.spriteGroup),
                       Bullet(Shotgun.SPEED, Shotgun.RANGE, Shotgun.DAMAGE, self.rect.centerx, self.rect.centery, self.horizontal, 5, self.wallsSpriteGroup, self.spriteGroup)]
            return bullets


#Long range with high damage
class Sniper(Weapon):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/weapons/sniper/", "sniper40x40a.png"))
    ANIMATION = ""
    FLASH = pygame.image.load(os.path.join("assets/weapons/sniper/", "sniper40x40b.png"))
    DAMAGE = 4
    MAX_AMMO = 3
    RANGE = 500
    SPEED = 40

    def __init__(self, x, y, powerupSpriteGroup, wallsSpriteGroup, *spriteGroup):
        super(Sniper, self).__init__(x, y, Sniper.SIZE, Sniper.IMAGE, Sniper.ANIMATION, pygame.mixer.Sound("assets/sounds/sniper.wav"), Sniper.MAX_AMMO, powerupSpriteGroup, wallsSpriteGroup, *spriteGroup)
        self.ammoImage = pygame.image.load(os.path.join("assets/weapons/sniper/", "round40x40.png"))
        self.ammo = Sniper.MAX_AMMO
        self.flashImage = Sniper.FLASH

    def shoot(self):
        if self.ammo > 0:
            self.ammo -= 1
            self.shotSound.play()
            self.flash()
            return Bullet(Sniper.SPEED, Sniper.RANGE, Sniper.DAMAGE, self.rect.centerx, self.rect.centery, self.horizontal, 0, self.wallsSpriteGroup, self.spriteGroup),


