import pygame, sys, os
from classes.sprite import Sprite
sys.path.append(os.getcwd() + "\\classes")
sys.path.append(os.getcwd() + "\\")


class Bullet(Sprite):

    SIZE = 5
    IMAGE = pygame.image.load(os.path.join("assets/player/", "bullet.png"))
    ANIMATION = ""

    def __init__(self, speed, shootDistance, damage, x, y, direction, vector, wallsSpriteGroup, *groups):
        super(Bullet, self).__init__(x, y, Bullet.SIZE, Bullet.IMAGE, Bullet.ANIMATION, *groups)
        self.damage = damage
        self.speed = speed
        self.shootDistance = shootDistance
        self.originalX = x
        self.temporaryX = 0
        self.vector = vector
        self.direction = direction
        self.wallsSpriteGroup = wallsSpriteGroup
        if direction == "left":
            self.image = pygame.transform.flip(self.image, True, False)

    def fly(self):
        if self.direction == "left":
                self.move(-self.speed, self.vector)
        elif self.direction == "right":
                self.move(self.speed, self.vector)

        # kill bullet if it is not moving anymore or reached its range
        durationX = self.originalX - self.rect.x
        if self.temporaryX == self.rect.x or durationX > self.shootDistance or durationX < -self.shootDistance:
            self.kill()
        self.temporaryX = self.rect.x

