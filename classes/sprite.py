import pygame, math


class Sprite(pygame.sprite.Sprite):

    def __init__(self, x, y, size, image, animation, *groups):
        super().__init__(*groups)
        self.rect = pygame.Rect(x, y, size, size)
        self.spriteGroup = groups
        self.horizontal = "right"
        self.vertical = "up"
        self.image = image
        self.collision = False
        self.index = 0
        self.images = animation
        self.isMoving = True
        self.movable = True
        self.asset = None

    def __str__(self):
        return self.__class__.__name__

    def flipHorizontaly(self, horizontal):
        if self.horizontal == horizontal:
            return
        if self.isMoving and (self.horizontal == "left" or self.horizontal == "right"):
            self.horizontal = horizontal
            for image in range(len(self.images)):
                self.images[image] = pygame.transform.flip(self.images[image], True, False)
                if self.asset and self.horizontal != self.asset.horizontal:
                    self.asset.image = pygame.transform.flip(self.asset.image, True, False)
                    self.asset.horizontal = horizontal

    def flipVerticaly(self, vertical):
        if self.vertical == vertical:
            return
        if self.vertical == "up" or self.vertical == "down":
            self.image = pygame.transform.flip(self.image, False, True)
            self.vertical = vertical

    def move(self, horizontal, vertical):
        if horizontal != 0:
            self.moveSingleAxis(horizontal, 0)
        if vertical != 0:
            self.moveSingleAxis(0, vertical)

    def moveSingleAxis(self, horizontal, vertical):
        self.rect.x += horizontal
        self.rect.y += vertical
        self.collision = False
        self.isMoving = True

        if pygame.sprite.spritecollideany(self, self.wallsSpriteGroup, False):
            wall = pygame.sprite.spritecollideany(self, self.wallsSpriteGroup, False)
            if horizontal > 0:
                self.rect.right = wall.rect.left
            if horizontal < 0:
                self.rect.left = wall.rect.right
            if vertical > 0:
                self.rect.bottom = wall.rect.top
            if vertical < 0:
                self.rect.top = wall.rect.bottom
            self.collision = True
            self.isMoving = False

        if self.isMoving:
            if horizontal < 0:
                self.flipHorizontaly("left")
            elif horizontal > 0:
                self.flipHorizontaly("right")

    def update(self):
        if self.images and (self.isMoving or not self.movable):
            self.index += 0.16
            if self.index >= len(self.images):
                self.index = 0
            self.image = self.images[math.floor(self.index)]

        # add asset to sprite - weapon, heat, armor, etc.
        if self.asset:
            self.asset.rect.centerx = self.rect.centerx
            self.asset.rect.centery = self.rect.centery
