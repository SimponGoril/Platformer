import pygame, sys, os, random
from classes.sprite import Sprite
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Enemy(Sprite):
    DIRECTIONS = ["right", "left", "up", "down"]

    def __init__(self, x, y, size, bounty, speed, health, contactDamage, image, animation, sound, wallsSpriteGroup, *spriteGroup):
        super(Enemy, self).__init__(x, y, size, image, animation, *spriteGroup)
        self.bounty = bounty
        self.speed = speed
        self.direction = "right"
        self.wallsSpriteGroup = wallsSpriteGroup
        self.dieSound = sound
        self.maxHealth = health
        self.health = health
        self.contactDamage = contactDamage

    def traverse(self):
        if self.collision:
            self.direction = Enemy.DIRECTIONS[random.randint(0, 3)]

        if self.direction == "right":
            self.move(self.speed, 0)
        if self.direction == "left":
            self.move(-self.speed, 0)
        if self.direction == "up":
            self.move(0, self.speed)
        if self.direction == "down":
            self.move(0, -self.speed)

    #Return true if enemy died
    def takeDamage(self, attack):
        self.health -= attack
        if self.health <= 0:
            self.die()
            return True

    def die(self):
        self.dieSound.play()
        self.kill()

    def seek(self, player):
        #horizontal movement
        if self.rect.x > player.rect.x:
            #left
            self.move(-self.speed, 0)
        elif (self.rect.x - player.rect.x + player.rect.width) < 0:
            #right
            self.move(self.speed, 0)

        #vertical movement
        if self.rect.y > player.rect.y:
            #down
            self.move(0, -self.speed)
        elif (self.rect.y - player.rect.y + player.rect.height) < 0:
            #up
            self.move(0, self.speed)

    def avoid(self, player):
        #horizontal movement
        if self.rect.x > player.rect.x:
            self.move(self.speed, 0)
        else:
            self.move(-self.speed, 0)

        #vertical movement
        if self.rect.y > player.rect.y:
            self.move(0, self.speed)
        else:
            self.move(0, -self.speed)


#basic enemy
class Grunt(Enemy):
    IMAGE = pygame.image.load(os.path.join("assets/enemy/", "grunt40x40a.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/enemy/", "grunt40x40a.png")),
                 pygame.image.load(os.path.join("assets/enemy/", "grunt40x40b.png")),
                 pygame.image.load(os.path.join("assets/enemy/", "grunt40x40c.png"))]
    CORPSE = pygame.image.load(os.path.join("assets/enemy/", "corpseA40x40.png"))
    SIZE = 40
    HEALTH = 2
    SPEED = 5
    DAMAGE = 1
    BOUNTY = 100

    def __init__(self, x, y, wallsSpriteGroup, *spriteGroup):
        super(Grunt, self).__init__(x, y, Grunt.SIZE, Grunt.BOUNTY, Grunt.SPEED, Grunt.HEALTH, Grunt.DAMAGE, Grunt.IMAGE, Grunt.ANIMATION, pygame.mixer.Sound("assets/sounds/death.wav"), wallsSpriteGroup, *spriteGroup)
        self.corpse = Grunt.CORPSE


#basic enemy projectile
class Slime(Enemy):
    IMAGE = pygame.image.load(os.path.join("assets/enemy/", "slime40x40.png"))
    SIZE = 20
    HEALTH = 1
    ANIMATION = ""
    SPEED = 20
    DAMAGE = 1
    BOUNTY = 0

    def __init__(self, x, y, direction, wallsSpriteGroup, *spriteGroup):
        super(Slime, self).__init__(x, y, Slime.SIZE, Slime.BOUNTY, Slime.SPEED, Slime.HEALTH, Slime.DAMAGE, Slime.IMAGE, Slime.ANIMATION, pygame.mixer.Sound("assets/sounds/death.wav"), wallsSpriteGroup, *spriteGroup)
        self.direction = direction

    def traverse(self):
        if self.direction == "right":
            self.move(self.speed, 0)
        if self.direction == "left":
            self.move(-self.speed, 0)
        if self.direction == "up":
            self.move(0, self.speed)
        if self.direction == "down":
            self.move(0, -self.speed)
        if self.collision:
            self.kill()


#slow but tough enemy
class Sarge(Enemy):
    IMAGE = pygame.image.load(os.path.join("assets/enemy/", "sarge40x40a.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/enemy/", "sarge40x40a.png")),
                 pygame.image.load(os.path.join("assets/enemy/", "sarge40x40b.png")),
                 pygame.image.load(os.path.join("assets/enemy/", "sarge40x40c.png"))]
    CORPSE = pygame.image.load(os.path.join("assets/enemy/", "corpseB40x40.png"))
    SIZE = 40
    HEALTH = 4
    SPEED = 3
    DAMAGE = 1
    BOUNTY = 250

    def __init__(self, x, y, wallsSpriteGroup, *spriteGroup):
        super(Sarge, self).__init__(x, y, Sarge.SIZE, Sarge.BOUNTY, Sarge.SPEED, Sarge.HEALTH, Sarge.DAMAGE, Sarge.IMAGE, Sarge.ANIMATION, pygame.mixer.Sound("assets/sounds/death.wav"), wallsSpriteGroup, *spriteGroup)
        self.spitSound = pygame.mixer.Sound("assets/sounds/spit.wav")
        self.charge = 0
        self.corpse = Sarge.CORPSE

    def traverse(self):
        super().traverse()
        if self.charge >= 100:
            self.charge = 0
            return self.spit()
        else:
            self.charge += random.randint(1, 3)

    def spit(self):
        self.spitSound.play()
        return Slime(self.rect.centerx - 10, self.rect.centery - 20, self.direction, self.wallsSpriteGroup, self.spriteGroup)


#fast but weak enemy, gets faster between wall collision
class Rat(Enemy):
    IMAGE = pygame.image.load(os.path.join("assets/enemy/", "rat40x40a.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/enemy/", "rat40x40a.png")),
                 pygame.image.load(os.path.join("assets/enemy/", "rat40x40b.png")),
                 pygame.image.load(os.path.join("assets/enemy/", "rat40x40c.png"))]
    CORPSE = pygame.image.load(os.path.join("assets/enemy/", "corpseC40x40.png"))
    SIZE = 40
    HEALTH = 1
    SPEED = 6
    DAMAGE = 1
    BOUNTY = 50

    def __init__(self, x, y, wallsSpriteGroup, *spriteGroup):
        super(Rat, self).__init__(x, y, Rat.SIZE, Rat.BOUNTY, Rat.SPEED, Rat.HEALTH, Rat.DAMAGE, Rat.IMAGE, Rat.ANIMATION, pygame.mixer.Sound("assets/sounds/death.wav"), wallsSpriteGroup, *spriteGroup)
        self.corpse = Rat.CORPSE

    def traverse(self):
        if not self.collision:
            self.speed += 0.13
        else:
            self.speed = Rat.SPEED
        super().traverse()


#fast but weak enemy, gets faster between wall collision
class Brain(Enemy):
    IMAGE = pygame.image.load(os.path.join("assets/enemy/", "brain200x200a.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/enemy", "brain200x200a.png")),
                 pygame.image.load(os.path.join("assets/enemy", "brain200x200b.png")),
                 pygame.image.load(os.path.join("assets/enemy", "brain200x200c.png"))]
    CORPSE = pygame.image.load(os.path.join("assets/enemy/", "corpseD200x200.png"))
    SIZE = 200
    HEALTH = 20
    SPEED = 3
    DAMAGE = 3
    BOUNTY = 1000

    def __init__(self, x, y, wallsSpriteGroup, *spriteGroup):
        super(Brain, self).__init__(x, y, Brain.SIZE, Brain.BOUNTY, Brain.SPEED, Brain.HEALTH, Brain.DAMAGE, Brain.IMAGE, Brain.ANIMATION, pygame.mixer.Sound("assets/sounds/death.wav"), wallsSpriteGroup, *spriteGroup)
        self.spitSound = pygame.mixer.Sound("assets/sounds/spit.wav")
        self.charge = 0
        self.corpse = Brain.CORPSE

    def traverse(self):
        super().traverse()
        if self.charge >= 100:
            self.charge = 0
            return self.megaSpit()
        else:
            self.charge += random.randint(3, 4)

    def megaSpit(self):
        self.spitSound.play()
        whereToShoot = random.randint(0, 3)
        if whereToShoot <= 1:
            slimes = [Slime(self.rect.centerx, self.rect.centery - 60, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup),
                      Slime(self.rect.centerx, self.rect.centery + 30, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup),
                      Slime(self.rect.centerx, self.rect.centery, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup),
                      Slime(self.rect.centerx, self.rect.centery - 30, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup)]
        else:
            slimes = [Slime(self.rect.centerx - 60, self.rect.centery, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup),
                      Slime(self.rect.centerx + 30, self.rect.centery, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup),
                      Slime(self.rect.centerx, self.rect.centery, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup),
                      Slime(self.rect.centerx - 30, self.rect.centery, Enemy.DIRECTIONS[whereToShoot], self.wallsSpriteGroup, self.spriteGroup)]
        return slimes



