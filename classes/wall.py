import pygame, sys, os, random
from classes.sprite import Sprite
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Wall(Sprite):

    SIZE = 40
    SPEED = 20
    IMAGES = [pygame.image.load(os.path.join("assets/environment/", "wall40x40a.png")),
              pygame.image.load(os.path.join("assets/environment/", "wall40x40b.png")),
              pygame.image.load(os.path.join("assets/environment/", "wall40x40c.png"))]
    ANIMATION = ""

    def __init__(self, x, y, *groups):
        super(Wall, self).__init__(x, y, Wall.SIZE, Wall.IMAGES[random.randint(0, len(Wall.IMAGES) - 1)], Wall.ANIMATION, *groups)
