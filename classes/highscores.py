
import sys, os, base64
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Highscores:

    @staticmethod
    def loadHighScores():
        scores = []
        try:
            file = open("score.sco","r")
        except FileNotFoundError:
            print("Unable to found score file, creating one now")
            file = open("score.sco", "w")
            file.close()
            return scores
        #prepare score string from file
        scoreString = base64.b64decode(file.readline()).decode("utf-8")
        #prepare score list
        scoreList = scoreString.split(",")
        #iterate through it and decode it with base64
        decryptedScores = list(map(lambda x: base64.b64decode(x).decode("utf-8"), scoreList))
        #cast score list members to integer and sort
        scores = list(map(int, decryptedScores))
        scores.sort(reverse=True)
        file.close()
        return scores

    @staticmethod
    def saveHighScores(scores):
        file = open("score.sco","w")
        #parse score as list of strings
        scoresStringList = list(map(str, scores[:10]))
        #iterate through it and encode it with base64
        encryptedScoresList = list(map(lambda x: base64.b64encode(bytes(x.encode("utf-8"))).decode("utf-8"), scoresStringList))
        #prepare string to write in the scores file
        scoresString = ",".join(list(map(str, encryptedScoresList)))
        #encode it again to final string
        encodedString = base64.b64encode(bytes(scoresString.encode("utf-8"))).decode("utf-8")
        file.write(str(encodedString))
        file.close()
