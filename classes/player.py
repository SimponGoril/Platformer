import pygame, sys, os
from classes.sprite import Sprite
from classes.weapon import Weapon
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\assets\\ ")


class Player(Sprite):

    SIZE = 40
    SPEED = 10
    IMAGE = pygame.image.load(os.path.join("assets/player/", "indy40x40a.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/player/", "indy40x40a.png")),
                 pygame.image.load(os.path.join("assets/player/", "indy40x40b.png")),
                 pygame.image.load(os.path.join("assets/player/", "indy40x40c.png"))]

    def __init__(self, x, y, wallsSpriteGroup, *spriteGroup):
        super(Player, self).__init__(x, y, Player.SIZE, Player.IMAGE, Player.ANIMATION, *spriteGroup)
        self.speed = Player.SPEED
        self.wallsSpriteGroup = wallsSpriteGroup
        self.health = 2
        self.score = 0
        self.inventory = []

    def acquireWeapon(self, weapon):
        for asset in self.inventory:
            if type(asset) is type(weapon):
                weapon.kill()
                return
        self.inventory.append(weapon)
        if not self.asset:
            self.asset = weapon
        else:
            self.orientHideAndEquipWeapon(weapon)

    def selectWeapon(self, number):
        if len(self.inventory) >= number:
            self.orientHideAndEquipWeapon(self.inventory[number - 1])
            print("Selecting weapon: %s" % self.asset)

    def orientHideAndEquipWeapon(self, weapon):
        # orient weapon if necessary
        if weapon.horizontal != self.horizontal:
            weapon.image = pygame.transform.flip(weapon.image, True, False)
            weapon.horizontal = self.horizontal
        # hide from drawboard
        self.asset.rect.centerx = 850
        # equip
        self.asset = weapon

    def shoot(self):
        if isinstance(self.asset, Weapon):
            return self.asset.shoot()
        else:
            return

    def resetStats(self):
        self.health = 2

    def kill(self):
        super(Player, self).kill()
        if isinstance(self.asset, Sprite):
            self.asset.kill()
