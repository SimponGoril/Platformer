import pygame, sys, os
from classes.sprite import Sprite
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Powerup(Sprite):

    def __init__(self, x, y, size, image, animation, *spriteGroup):
        super(Powerup, self).__init__(x, y, size, image, animation, *spriteGroup)
        self.obtainSound = pygame.mixer.Sound("assets/sounds/obtain.wav")

    def obtained(self):
        self.obtainSound.play()


class Key(Powerup):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/items/key/", "key40x40.png"))
    ANIMATION = ""

    def __init__(self, x, y, *spriteGroup):
        super(Key, self).__init__(x, y, Key.SIZE, Key.IMAGE, Key.ANIMATION, *spriteGroup)


class Health(Powerup):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/items/heart", "heart40x40.png"))
    ANIMATION = ""

    def __init__(self, x, y, *spriteGroup):
        super(Health, self).__init__(x, y, Health.SIZE, Health.IMAGE, Health.ANIMATION, *spriteGroup)


class Ammo(Powerup):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/items/ammo", "ammo40x40.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/items/ammo", "ammo40x40a.png")),
                 pygame.image.load(os.path.join("assets/items/ammo", "ammo40x40b.png")),
                 pygame.image.load(os.path.join("assets/items/ammo", "ammo40x40c.png")),
                 pygame.image.load(os.path.join("assets/items/ammo", "ammo40x40d.png"))]

    def __init__(self, x, y, *spriteGroup):
        super(Ammo, self).__init__(x, y, Ammo.SIZE, Ammo.IMAGE, Ammo.ANIMATION, *spriteGroup)
        self.movable = False


class Cash(Powerup):

    SIZE = 40
    IMAGE = pygame.image.load(os.path.join("assets/items/cash", "cash40x40.png"))
    ANIMATION = [pygame.image.load(os.path.join("assets/items/cash", "cash40x40a.png")),
                 pygame.image.load(os.path.join("assets/items/cash", "cash40x40b.png")),
                 pygame.image.load(os.path.join("assets/items/cash", "cash40x40c.png")),
                 pygame.image.load(os.path.join("assets/items/cash", "cash40x40d.png"))]

    def __init__(self, x, y, *spriteGroup):
        super(Cash, self).__init__(x, y, Cash.SIZE, Cash.IMAGE, Cash.ANIMATION, *spriteGroup)
        self.obtainSound = pygame.mixer.Sound("assets/sounds/money.wav")
        self.movable = False
        self.value = 300

