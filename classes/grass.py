import pygame, sys, os, random
from classes.sprite import Sprite
sys.path.append(os.getcwd() + "\\source\\classes")
sys.path.append(os.getcwd() + "\\")


class Grass(Sprite):

    SIZE = 40
    IMAGE = [pygame.image.load(os.path.join("assets/environment/", "grass40x40a.png")),
             pygame.image.load(os.path.join("assets/environment/", "grass40x40b.png")),
             pygame.image.load(os.path.join("assets/environment/", "grass40x40c.png")),
             pygame.image.load(os.path.join("assets/environment/", "grass40x40d.png"))]
    ANIMATION = ""

    def __init__(self, x, y, *spriteGroup):
        super(Grass, self).__init__(x, y, Grass.SIZE, Grass.IMAGE[random.randint(0, len(Grass.IMAGE) - 1)], Grass.ANIMATION, *spriteGroup)
