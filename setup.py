from cx_Freeze import setup, Executable

includefiles = ["assets/", "classes/"]
includes = []
excludes = []
packages = []

setup(
      name="Platformer",
      version="0.5",
      description="A platformer",
      author="Simpon",
      options={"build_exe": {"build_exe": ".", "includes": includes, "excludes": excludes, "packages": packages,
                               "include_files": includefiles}},
      executables=[Executable(script="game.py", icon="assets/icon32x32.ico")], requires=["pygame"]
)
